# tsm-datastore-lib

- This library provides functionality to persist time series data via sqlalchemy to a database
- The data model is a simple adaption of the OGC sensorthings API data model (Thing, Datastream, Observation)

## Getting Started

- add the following line in your requirements.txt
- (the token will not be necessary in the future when all code gets open source)


> git+https://pip-install-token:hWfT2hJzyP1CBVLSVNSW@git.ufz.de/rdm-software/timeseries-management/tsm-datastore-lib@main#egg=tsm-datastore-lib

- imports e.g.:


> import Datastore 
> 
> from Datastore import Observation

- save data like this:


> datastore = Datastore.get_datastore(
>     f"{driver}://{user}:{password}@{host}/{db}", "ce2b4fb6-d9de-11eb-a236-125e5a40a845"
> )
>   
> observation = Observation(timestamp, float_value, 'test', 1)
>   
> datastore.store_observation(observation)
> 
> datastore.finalize()


## Ressources

- SQLAlchemy Quick Start: https://gist.github.com/DmitryBe/805fb35e3472b8985c654c8dfb8aa127
- Python Packaging:
    - https://packaging.python.org/tutorials/packaging-projects/
    - https://setuptools.readthedocs.io/en/latest/userguide/dependency_management.html?highlight=install_requires
