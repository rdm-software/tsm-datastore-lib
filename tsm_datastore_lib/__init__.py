from __future__ import annotations
import re
import uuid

from .version import __version__
from .SqlAlchemyDatastore import SqlAlchemyDatastore
from .Observation import Observation
from .AbstractDatastore import AbstractDatastore


def get_datastore(uri: str, device_id: str | int | uuid.UUID) -> SqlAlchemyDatastore:
    datastore = None

    # Postgres uri
    if re.search("^postgres://", uri):
        datastore = SqlAlchemyDatastore(uri, device_id)

    # Postgresql uri
    if re.search("^postgresql://", uri):
        datastore = SqlAlchemyDatastore(uri, device_id)

    # ORACLE uri
    if re.search("^oracle://", uri):
        datastore = SqlAlchemyDatastore(uri, device_id)

    # default
    if datastore is None:
        raise NotImplementedError

    return datastore
