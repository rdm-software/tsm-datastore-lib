from __future__ import annotations

import datetime
import logging
import uuid

from typing import List, Union
from urllib.parse import urlparse
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.dialects.postgresql import insert

from .AbstractDatastore import AbstractDatastore
from .Observation import Observation
from .JournalEntry import JournalEntry
from .SqlAlchemy.Model import Thing, Datastream
from .SqlAlchemy.Model.Observation import Observation as SqlaObservation, ResultType
from .SqlAlchemy.Model.Journal import Journal as SqlaJournal


logger = logging.getLogger("tsm_datastore_lib")
logger.addHandler(logging.NullHandler())


class DatastreamNotFoundError(Exception):
    def __init__(self, thing_uuid, position) -> None:
        super().__init__(
            f"Thing with with uuid '{thing_uuid}' "
            f"has no datastream at position {position}."
        )


class ThingNotFoundError(Exception):
    def __init__(self, thing_uuid) -> None:
        super().__init__(f"No thing with uuid '{thing_uuid}' found in database.")


class SqlAlchemyDatastore(AbstractDatastore):
    CHUNK_SIZE = 1000

    def __init__(self, uri: str, device_id: uuid.UUID, schema: str = ""):
        self.schema = schema
        self.sqla_datastream_cache = {}
        self.session: Session = None
        self.sqla_thing: Thing = None
        self.chunk = []
        self.current_chunk_idx = 0

        super().__init__(uri, device_id, schema)

    def get_parser_parameters(self, parser_type: str) -> dict:
        return self.sqla_thing.get_parser_parameters(parser_type)

    def initiate_connection(self, schema: str) -> None:
        hostname = urlparse(self.uri).hostname
        logger.info(f"Connecting to database {hostname!r}")
        engine = create_engine(self.uri, future=True)
        schema_engine = engine.execution_options(
            schema_translate_map={"per_user": schema}
        )
        self.engine = engine
        session = sessionmaker(bind=schema_engine)
        self.session = session()
        logger.info(f"Successfully connected to {hostname}")

        self.sqla_thing = (
            self.session.query(Thing).filter(Thing.uuid == str(self.device_id)).first()
        )
        if self.sqla_thing is None:
            raise ThingNotFoundError(self.device_id)

    def _store(self, obj) -> None:
        """
        Store and commit the batch when max chunk size is reached.
        """
        self.chunk.append(obj)
        self.current_chunk_idx += 1
        if self.current_chunk_idx % SqlAlchemyDatastore.CHUNK_SIZE == 0:
            self.insert_commit_chunk()

    def store_observation(self, observation: Observation) -> None:
        sqla_datastream = self.fetch_or_create_datastream(observation)
        sqla_obs = {
            "result_time": observation.timestamp,
            "datastream_id": sqla_datastream.id,
            "parameters": {
                "origin": observation.origin,
                "column_header": observation.header,
            },
            "result_number": None,
            "result_string": None,
            "result_boolean": None,
        }
        if isinstance(observation.value, (float, int)):
            sqla_obs["result_number"] = observation.value
            sqla_obs["result_type"] = ResultType.Number
        elif isinstance(observation.value, str):
            sqla_obs["result_string"] = observation.value
            sqla_obs["result_type"] = ResultType.String
        elif isinstance(observation.value, bool):
            sqla_obs["result_boolean"] = observation.value
            sqla_obs["result_type"] = ResultType.Bool
        else:
            raise ValueError(f"datatype not supported: {type(observation.value)}")
        self._store(sqla_obs)

    def store_journal_entry(self, entry: JournalEntry) -> None:
        sqla_journal_entry = SqlaJournal(
            timestamp=entry.timestamp,
            level=entry.timestamp,
            message=entry.message,
            extra=entry.extra,
            thing=self.sqla_thing,
        )
        self._store(sqla_journal_entry)

    def update_observation_quality(
        self,
        observation: Observation,
        quality: Union[dict, list, str, int, float, bool, None],
    ) -> None:
        """Set or overwrite a quality value for an observation."""

        # ensure existence of values we might update
        if self.chunk:
            self.insert_commit_chunk()

        datastream = self.get_datastream(observation.position)
        self.session.query(SqlaObservation).filter(
            SqlaObservation.datastream == datastream,
            SqlaObservation.result_time == observation.timestamp,
        ).update({"result_quality": quality})

    def store_observations(self, observations: List[Observation]) -> None:
        for o in observations:
            self.store_observation(o)

    def store_journal_entries(self, entries: List[JournalEntry]) -> None:
        for e in entries:
            self.store_journal_entry(e)

    def get_datastream(self, position: int | str) -> Datastream:
        """
        Get a datastream of a thing.

        Parameters
        ----------
        position : int
            Position of the datastream.

        Returns
        -------
        datastream: Datastream

        Raises
        ------
        DatastreamNotFoundError: if thing has no datastream at given position
        """
        stream = self._fetch_datastream(position)
        if stream is None:
            raise DatastreamNotFoundError(self.sqla_thing, position)
        return stream

    def fetch_or_create_datastream(self, observation: Observation) -> Datastream:

        stream = self._fetch_datastream(observation.position)
        if stream is None:
            stream = self._create_datastream(observation.position)
        return stream

    def _fetch_datastream(self, position: int | str) -> Datastream | None:
        # used as name in data stream model and as key for a simple cache
        # as this will not change during one run of the extractor
        name = f"{self.sqla_thing.name}/{position}"

        # Lookup simple cache at first
        stream = self.sqla_datastream_cache.get(name, None)

        # cache miss
        if stream is None:
            stream = (
                self.session.query(Datastream)
                .filter(
                    Datastream.position == str(position),
                    Datastream.thing == self.sqla_thing,
                )
                .first()
            )

            # update cache
            if stream is not None:
                self.sqla_datastream_cache[name] = stream

        return stream

    def _create_datastream(self, position: int | str):
        name = f"{self.sqla_thing.name}/{position}"
        stream = Datastream(
            thing=self.sqla_thing,
            position=str(position),
            name=name,
            properties={"created_at": str(datetime.datetime.now())},
        )
        self.session.add(stream)
        self.session.commit()
        # update cache
        self.sqla_datastream_cache[name] = stream
        return stream

    def insert_commit_chunk(self):
        if len(self.chunk) > 0:
            # if we are building query statements directly, instead of using
            # the session magic (we have to if we want to use on_conflict_*)
            # the schema mapping does not work, that's why we need to set the
            # schema explictly
            old_schema = SqlaObservation.__table__.schema
            SqlaObservation.__table__.schema = self.schema

            try:
                # NOTE:
                # an on_conflict_do_update would probably more appropiate, but is
                # harder to achieve. As we intend to replace the tsm_datastore_lib
                # anyways, I would prefer to not spend more time here and live  with
                # the non optimal but simpler solution for now...
                stmt = (insert(SqlaObservation)
                        .values(self.chunk)
                        .on_conflict_do_nothing())

                with self.engine.connect() as conn:
                    conn.execute(stmt)
                    conn.commit()
            finally:
                SqlaObservation.__table__.schema = old_schema

            self.chunk.clear()
            logger.debug(
                f"Pushed {self.current_chunk_idx} new observations to database."
            )

    def finalize(self):
        # insert last chunk
        self.insert_commit_chunk()
        self.current_chunk_idx = 0
        logger.info("Close database session.")
        self.session.close()
        self.session = None

    def __del__(self):
        if self.session is not None:
            self.finalize()
