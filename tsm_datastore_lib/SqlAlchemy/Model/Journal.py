# coding: utf-8
import sqlalchemy_jsonfield
from sqlalchemy import (
    BigInteger,
    Column,
    ForeignKey,
    String,
    Text,
    text,
    UniqueConstraint,
    DateTime,
)
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from tsm_datastore_lib.SqlAlchemy.Model import Thing

Base = declarative_base()
metadata = Base.metadata


class Journal(Base):
    __tablename__ = "journal"
    __table_args__ = {"schema": "per_user"}

    id = Column(
        BigInteger,
        primary_key=True,
        server_default=text("nextval('journal_id_seq'::regclass)"),
    )
    timestamp = Column(DateTime(True), nullable=False)
    level = Column(String(30), nullable=False)
    message = Column(Text)
    extra = Column(sqlalchemy_jsonfield.JSONField())
    thing_id = Column(
        ForeignKey(Thing.id, deferrable=True, initially="DEFERRED"),
        nullable=False,
        index=True,
    )

    thing = relationship(Thing)
