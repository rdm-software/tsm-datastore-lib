import datetime


class JournalEntry:
    """
    Represent an entry in the Journal.

    Attributes:
        timestamp (datetime): Timestamp of the journal entry. The point in time, where the entry was created.
        level (str):  The severity of the message.
        message (str): The (log) message.
        extra (dict): Extra information, for example a stacktrace. Must be json-serializable.
    """

    def __init__(self, timestamp: datetime, level: float, message: str, extra: dict):
        self.timestamp = timestamp
        self.level = level
        self.message = message
        self.extra = extra

    def __repr__(self):
        return f"JournalEntry({self.timestamp} | {self.level} | {self.message} | {self.extra})"
