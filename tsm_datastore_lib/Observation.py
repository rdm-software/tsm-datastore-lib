from __future__ import annotations
import datetime
import math



class Observation:
    """Base class of observation, may be a sqlalchemy model or some adapter class

    Attributes:
        timestamp (datetime): Timestamp of the data point. The point in time, where the value was
            measured.
        value (float | int | str | bool):  The measured value.
        origin (str):   The source of the raw data file.
        position (int): Position in the source file, i.g. number of the column in a csv file.
    """

    def __init__(
        self,
        timestamp: datetime,
        value: float | int | str | bool,
        origin: str,
        position: int,
        header: str = "",
    ):
        self.timestamp = timestamp
        self.value = value
        self.origin = origin
        self.position = position
        self.header = header

        self.check_value()

    def check_value(self):
        # do not allow python nan for now
        if self.value is None:
            raise NoneNotAllowedHereError("None is not allowed as observation value.")
        if isinstance(self.value, (float, int)) and math.isnan(self.value):
            raise NanNotAllowedHereError("NaN is not allowed as observation value.")

    def __repr__(self):
        return f"Observation({self.timestamp} | {self.value} | {self.origin} | {self.position})"


class NanNotAllowedHereError(ValueError):
    pass

class NoneNotAllowedHereError(ValueError):
    pass
