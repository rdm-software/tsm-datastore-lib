#!/usr/bin/env python

MAJOR = 0
MINOR = 9
PATCH = 3
__version__ = "{}.{}.{}".format(MAJOR, MINOR, PATCH)
