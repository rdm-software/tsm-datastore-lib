from typing import Sequence, Dict, Any

import uuid
from abc import ABC, abstractmethod

from tsm_datastore_lib.Observation import Observation
from tsm_datastore_lib.JournalEntry import JournalEntry


class AbstractDatastore(ABC):
    def __init__(self, uri: str, device_id: uuid.UUID, schema: str = "") -> None:
        self.uri: str = uri
        self.device_id: uuid.UUID = device_id
        self.initiate_connection(schema)

    @abstractmethod
    def get_parser_parameters(self, parser_type) -> Dict[str, Any]:
        raise NotImplementedError

    @abstractmethod
    def initiate_connection(self, schema: str) -> None:
        """Connect to the database or instantiate http classes"""
        raise NotImplementedError

    @abstractmethod
    def store_observations(self, observations: Sequence[Observation]) -> None:
        """Save a bunch of observations to the datastore"""
        raise NotImplementedError

    @abstractmethod
    def store_journal_entries(self, entries: Sequence[JournalEntry]) -> None:
        """Save a bunch of journal entries to the datastore"""
        raise NotImplementedError

    @abstractmethod
    def finalize(self):
        """Do last things to do, e.g. commit to database."""
        raise NotImplementedError
