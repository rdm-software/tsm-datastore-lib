from setuptools import setup, find_packages

__version__ = None
# import the __version__
# this is a workaround to avoid importing the
# package itself here
with open("tsm_datastore_lib/version.py") as f:
    exec(f.read())

if __version__ is None:
    raise ValueError("version is not specified.")

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="tsm_datastore_lib",
    version=__version__,
    author="Martin Abbrent",
    author_email="martin.abbrent@ufz.de",
    description="Library for storing time series data into various types of databases",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ufz.de/rdm-software/tsm-datastore-lib",
    project_urls={
        "Bug Tracker": "https://git.ufz.de/rdm-software/tsm-datastore-lib/-/issues",
    },
    license="HEESIL",
    packages=find_packages(),
    install_requires=[
        "sqlalchemy~=1.4.34",
        "psycopg2-binary~=2.9.3",
        "humanfriendly~=10.0",
        "cx_Oracle~=8.3.0",
        "SQLAlchemy-JSONField~=1.0.0",
    ],
)
